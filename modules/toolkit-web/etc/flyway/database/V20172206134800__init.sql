CREATE TABLE feature_birthday (

  id            SERIAL        NOT NULL,

  occurs        DATE          NOT NULL,
  person        VARCHAR(255)  NOT NULL,

  CONSTRAINT pk_feature_birthday PRIMARY KEY (id)
)
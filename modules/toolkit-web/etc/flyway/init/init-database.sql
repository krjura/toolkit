CREATE DATABASE toolkit_web ENCODING 'UTF8';

CREATE USER toolkit_web WITH PASSWORD 'toolkit_web';

GRANT ALL PRIVILEGES ON DATABASE toolkit_web to toolkit_web;
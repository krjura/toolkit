package biz.db.dev.toolkit.web.controllers.pojo;

public class BirthdayAddRequest {

    private String occurs;

    private String person;

    public String getOccurs() {
        return occurs;
    }

    public void setOccurs(String occurs) {
        this.occurs = occurs;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}

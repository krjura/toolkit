package biz.db.dev.toolkit.web.model.repository.impl;

import biz.db.dev.toolkit.web.model.jooq.Tables;
import biz.db.dev.toolkit.web.model.jooq.tables.daos.FeatureBirthdayDao;
import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;
import biz.db.dev.toolkit.web.model.jooq.tables.records.FeatureBirthdayRecord;
import biz.db.dev.toolkit.web.model.repository.FeatureBirthdayRepository;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.RecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Repository
public class FeatureBirthdayRepositoryImpl implements FeatureBirthdayRepository {

    private static final FeatureBirthdayRecordMapper FEATURE_BIRTHDAY_RECORD_MAPPER = new FeatureBirthdayRecordMapper();

    @Autowired
    private Configuration configuration;

    @Autowired
    private DSLContext dslContext;

    private FeatureBirthdayDao dao;

    @PostConstruct
    public void init() {
        dao = new FeatureBirthdayDao(this.configuration);
    }

    @Override
    public FeatureBirthdayDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public FeatureBirthday save(FeatureBirthday featureBirthday) {
        FeatureBirthdayRecord record = dslContext
                .newRecord(Tables.FEATURE_BIRTHDAY);

        record.setOccurs(featureBirthday.getOccurs());
        record.setPerson(featureBirthday.getPerson());

        record.insert();

        return FEATURE_BIRTHDAY_RECORD_MAPPER.map(record);
    }

    private static class FeatureBirthdayRecordMapper implements RecordMapper<FeatureBirthdayRecord, FeatureBirthday> {

        @Override
        public FeatureBirthday map(FeatureBirthdayRecord record) {

            FeatureBirthday featureBirthday = new FeatureBirthday();
            featureBirthday.setId(record.getId());
            featureBirthday.setOccurs(record.getOccurs());
            featureBirthday.setPerson(record.getPerson());

            return featureBirthday;
        }
    }
}

package biz.db.dev.toolkit.web.controllers;

import biz.db.dev.toolkit.web.controllers.pojo.BirthdayAddRequest;
import biz.db.dev.toolkit.web.controllers.resources.FeatureBirthdayResource;
import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;
import biz.db.dev.toolkit.web.services.FeatureBirthdayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@Controller
public class BirthdayController {

    @Autowired
    private FeatureBirthdayService featureBirthdayService;

    @RequestMapping(
            path = "/api/v1/features/birthday",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FeatureBirthdayResource> add(@RequestBody BirthdayAddRequest request) {

        FeatureBirthday featureBirthday = this.featureBirthdayService
                .addBirthday(request.getOccurs(), request.getPerson());

        return ResponseEntity.ok(new FeatureBirthdayResource(featureBirthday));
    }

    @RequestMapping(
            path = "/api/v1/features/birthday/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FeatureBirthdayResource> get(
            @PathVariable( name = "id") Integer id) {

        Optional<FeatureBirthday> featureBirthdayOptional = this.featureBirthdayService.findById(id);

        return featureBirthdayOptional
                .map(featureBirthday -> ResponseEntity.ok(new FeatureBirthdayResource(featureBirthday)))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
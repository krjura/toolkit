package biz.db.dev.toolkit.web.services.impl;

import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;
import biz.db.dev.toolkit.web.model.repository.FeatureBirthdayRepository;
import biz.db.dev.toolkit.web.services.FeatureBirthdayService;
import biz.db.dev.toolkit.web.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class FeatureBirthdayServiceImpl implements FeatureBirthdayService {

    @Autowired
    private FeatureBirthdayRepository featureBirthdayRepository;

    @Override
    @Transactional
    public FeatureBirthday addBirthday(String occurs, String person) {

        FeatureBirthday featureBirthday = new FeatureBirthday();
        featureBirthday.setOccurs(FormatterUtils.toLocalDate(occurs));
        featureBirthday.setPerson(person);

        return featureBirthdayRepository.save(featureBirthday);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FeatureBirthday> findById(Integer id) {
        return Optional.ofNullable(featureBirthdayRepository.getDao().findById(id));
    }
}

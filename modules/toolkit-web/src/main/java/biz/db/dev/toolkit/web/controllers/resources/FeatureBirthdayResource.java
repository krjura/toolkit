package biz.db.dev.toolkit.web.controllers.resources;

import biz.db.dev.toolkit.web.controllers.pojo.FeatureBirthdayResponse;
import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;
import org.springframework.hateoas.Resource;

public class FeatureBirthdayResource extends Resource<FeatureBirthdayResponse> {

    public FeatureBirthdayResource(FeatureBirthday featureBirthday) {
        super(new FeatureBirthdayResponse(featureBirthday));
    }
}

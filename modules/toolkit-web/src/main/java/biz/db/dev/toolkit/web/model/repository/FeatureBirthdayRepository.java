package biz.db.dev.toolkit.web.model.repository;

import biz.db.dev.toolkit.web.model.jooq.tables.daos.FeatureBirthdayDao;
import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;

public interface FeatureBirthdayRepository {

    FeatureBirthdayDao getDao();

    FeatureBirthday save(FeatureBirthday featureBirthday);
}

package biz.db.dev.toolkit.web.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormatterUtils {

    private FormatterUtils() {
        // factory
    }

    public static String format(LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static LocalDate toLocalDate(String localDate) {
        return LocalDate.parse(localDate, DateTimeFormatter.ISO_LOCAL_DATE);
    }
}

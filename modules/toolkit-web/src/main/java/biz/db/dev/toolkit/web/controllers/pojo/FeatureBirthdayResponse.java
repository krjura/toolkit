package biz.db.dev.toolkit.web.controllers.pojo;

import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;
import biz.db.dev.toolkit.web.utils.FormatterUtils;

public class FeatureBirthdayResponse {

    private Integer id;

    private String occurs;

    private String person;

    public FeatureBirthdayResponse() {
        // reflections
    }

    public FeatureBirthdayResponse(FeatureBirthday featureBirthday ) {
        this.id = featureBirthday.getId();
        this.occurs = FormatterUtils.format(featureBirthday.getOccurs());
        this.person = featureBirthday.getPerson();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOccurs() {
        return occurs;
    }

    public void setOccurs(String occurs) {
        this.occurs = occurs;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}

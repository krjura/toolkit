package biz.db.dev.toolkit.web.services;

import biz.db.dev.toolkit.web.model.jooq.tables.pojos.FeatureBirthday;

import java.util.Optional;

public interface FeatureBirthdayService {

    FeatureBirthday addBirthday(String occurs, String person);

    Optional<FeatureBirthday> findById(Integer id);
}

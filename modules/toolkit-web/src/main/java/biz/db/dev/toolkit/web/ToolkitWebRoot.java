package biz.db.dev.toolkit.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@EnableDiscoveryClient
@ComponentScan( basePackageClasses = {
        ToolkitWebRoot.class
})
public class ToolkitWebRoot {

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(ToolkitWebRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }

    public static void main(String[] args) {
        ToolkitWebRoot application = new ToolkitWebRoot();
        application.runSpring(args);
    }
}

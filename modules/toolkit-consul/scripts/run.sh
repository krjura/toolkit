#!/bin/bash

APPLICATION_HOME=$(cd `dirname $0` && pwd)
cd "$APPLICATION_HOME"

./consul agent -config-file=consul.config
